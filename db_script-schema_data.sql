CREATE DATABASE  IF NOT EXISTS `pms` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `pms`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 195.113.118.25    Database: pms
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Comment`
--

DROP TABLE IF EXISTS `Comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comment` (
  `idComment` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` varchar(500) NOT NULL,
  `Task_idTask` int(11) NOT NULL,
  `User_idUser` int(11) NOT NULL,
  PRIMARY KEY (`idComment`),
  UNIQUE KEY `idComment_UNIQUE` (`idComment`),
  KEY `fk_Comment_Task1_idx` (`Task_idTask`),
  KEY `fk_Comment_User1_idx` (`User_idUser`),
  CONSTRAINT `fk_Comment_Task1` FOREIGN KEY (`Task_idTask`) REFERENCES `Task` (`idTask`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comment_User1` FOREIGN KEY (`User_idUser`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comment`
--

LOCK TABLES `Comment` WRITE;
/*!40000 ALTER TABLE `Comment` DISABLE KEYS */;
INSERT INTO `Comment` VALUES (3,'Chyba','<p>wrewerewr</p>',1,1),(5,'Chyba','<p>Posledn&iacute; test</p>',1,1),(6,'Chyba','<p>Prvn&iacute; komentar</p>',2,1),(9,'Chyba','<p>Tak to ale vyÅ?e&scaron;</p>',3,9),(12,'','<p>hsdkjahfkjahsdlkjf</p>',4,8),(13,'','<p><strong>sdf</strong></p>',4,8);
/*!40000 ALTER TABLE `Comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Priority`
--

DROP TABLE IF EXISTS `Priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Priority` (
  `idPriority` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`idPriority`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Priority`
--

LOCK TABLES `Priority` WRITE;
/*!40000 ALTER TABLE `Priority` DISABLE KEYS */;
INSERT INTO `Priority` VALUES (3,'Blocker'),(1,'Nízká'),(2,'Vysoká');
/*!40000 ALTER TABLE `Priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Project`
--

DROP TABLE IF EXISTS `Project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Project` (
  `idProject` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `startDate` datetime(6) NOT NULL,
  `endDate` datetime(6) DEFAULT NULL,
  `User_idUser` int(11) NOT NULL,
  `Priority_idPriority` int(11) DEFAULT NULL,
  `Status_idStatus` int(11) DEFAULT NULL,
  `Type_idType` int(11) DEFAULT NULL,
  `estimatedTime` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`idProject`),
  UNIQUE KEY `idProject_UNIQUE` (`idProject`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `fk_Project_User1_idx` (`User_idUser`),
  KEY `fk_Project_Priority1_idx` (`Priority_idPriority`),
  KEY `fk_Project_Status1_idx` (`Status_idStatus`),
  KEY `fk_Project_Type1_idx` (`Type_idType`),
  CONSTRAINT `fk_Project_Priority1` FOREIGN KEY (`Priority_idPriority`) REFERENCES `Priority` (`idPriority`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Project_Status1` FOREIGN KEY (`Status_idStatus`) REFERENCES `Status` (`idStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Project_Type1` FOREIGN KEY (`Type_idType`) REFERENCES `Type` (`idType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Project_User1` FOREIGN KEY (`User_idUser`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Project`
--

LOCK TABLES `Project` WRITE;
/*!40000 ALTER TABLE `Project` DISABLE KEYS */;
INSERT INTO `Project` VALUES (1,'Projekt1','<p>Projekt1</p>','2017-02-01 01:52:50.169000',NULL,9,3,1,2,0),(6,'Zkousim2','<p><strong>gssdgfsdsgdsgdsg</strong></p>','2017-02-01 12:50:25.491000',NULL,1,1,4,2,0),(9,'Projekt3','<p>ashfkjsl?fjlkasdjflkajsldkfj?lasdjflk</p>','2017-02-01 15:30:13.031000',NULL,10,NULL,NULL,NULL,0),(10,'Projekt 4 - novy test','<p>adshnfkjashdlfkjahskdjfhasdhlfkashdfkahsdfhlkj ashdlf hksfsdlkj l?kfashjdhlkjfasd hljsdgfhlkjfsduhl?kjfdgl?jksdgrho?jsdgrfhl?kjasdgflkshddvfgljhgdlkgjfsdaghkjlsdfgh?kjdhl?kjdgfh?kjsdfgh?kjsfdblkjhvfsdbhlkjgdyvfxljhkgasvdljghksdcahlkjgfasdlkjgfasdlgzuogasderuiotasRHLJGAswerlkjgAWRHLJAsdfhlkjgasfhdljgfasdLKJGasdLKADWuiogqGAwlohFASk</p>','2017-02-01 18:27:00.000000',NULL,9,3,1,1,0),(12,'Projekt 6','<p>asfjaslkfjksfjd?j?alsflk</p>','2017-02-01 20:20:00.000000','2017-02-01 20:22:47.267000',10,3,4,1,0),(13,'Projekt 7','<p>asfsdfasfasfasdf</p>','2017-02-01 20:42:00.000000','2017-02-01 20:48:51.283000',10,3,4,1,0);
/*!40000 ALTER TABLE `Project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `idRole` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idRole`),
  UNIQUE KEY `idRole_UNIQUE` (`idRole`),
  UNIQUE KEY `roleName_UNIQUE` (`roleName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role`
--

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` VALUES (1,'ROLE_ADMIN','admin'),(2,'ROLE_USER','user'),(3,'ROLE_MANAGER','manager');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Role_has_User`
--

DROP TABLE IF EXISTS `Role_has_User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role_has_User` (
  `Role_idRole` int(11) NOT NULL,
  `User_idUser` int(11) NOT NULL,
  PRIMARY KEY (`Role_idRole`,`User_idUser`),
  KEY `fk_Role_has_User_User1_idx` (`User_idUser`),
  KEY `fk_Role_has_User_Role_idx` (`Role_idRole`),
  CONSTRAINT `fk_Role_has_User_Role` FOREIGN KEY (`Role_idRole`) REFERENCES `Role` (`idRole`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Role_has_User_User1` FOREIGN KEY (`User_idUser`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role_has_User`
--

LOCK TABLES `Role_has_User` WRITE;
/*!40000 ALTER TABLE `Role_has_User` DISABLE KEYS */;
INSERT INTO `Role_has_User` VALUES (2,1),(2,8),(3,9),(1,10);
/*!40000 ALTER TABLE `Role_has_User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Status`
--

DROP TABLE IF EXISTS `Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Status` (
  `idStatus` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`idStatus`),
  UNIQUE KEY `idStatus_UNIQUE` (`idStatus`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Status`
--

LOCK TABLES `Status` WRITE;
/*!40000 ALTER TABLE `Status` DISABLE KEYS */;
INSERT INTO `Status` VALUES (1,'Otevřený'),(2,'Probíhá'),(4,'Uzavřený'),(3,'Vyřešený');
/*!40000 ALTER TABLE `Status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Task`
--

DROP TABLE IF EXISTS `Task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Task` (
  `idTask` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `startDate` datetime(6) NOT NULL,
  `endDate` datetime(6) DEFAULT NULL,
  `Priority_idPriority` int(11) NOT NULL,
  `Status_idStatus` int(11) NOT NULL,
  `Type_idType` int(11) NOT NULL,
  `estimatedTime` decimal(10,0) NOT NULL,
  `Project_idProject` int(11) NOT NULL,
  `User_idUser` int(11) NOT NULL,
  PRIMARY KEY (`idTask`),
  UNIQUE KEY `idTask_UNIQUE` (`idTask`),
  KEY `fk_Task_Priority1_idx` (`Priority_idPriority`),
  KEY `fk_Task_Status1_idx` (`Status_idStatus`),
  KEY `fk_Task_Type1_idx` (`Type_idType`),
  KEY `fk_Task_Project1_idx` (`Project_idProject`),
  KEY `fk_Task_User1_idx` (`User_idUser`),
  CONSTRAINT `fk_Task_Priority1` FOREIGN KEY (`Priority_idPriority`) REFERENCES `Priority` (`idPriority`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Task_Project1` FOREIGN KEY (`Project_idProject`) REFERENCES `Project` (`idProject`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Task_Status1` FOREIGN KEY (`Status_idStatus`) REFERENCES `Status` (`idStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Task_Type1` FOREIGN KEY (`Type_idType`) REFERENCES `Type` (`idType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Task_User1` FOREIGN KEY (`User_idUser`) REFERENCES `User` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Task`
--

LOCK TABLES `Task` WRITE;
/*!40000 ALTER TABLE `Task` DISABLE KEYS */;
INSERT INTO `Task` VALUES (1,'Task1','Task1','2017-01-30 22:14:05.486000',NULL,2,1,2,0,1,1),(2,'Task2','Blabla','2017-01-30 21:10:59.915000',NULL,1,1,1,10,1,1),(3,'Task 420 Blaze it','<p>Blellel</p>','2017-01-31 11:27:09.430000',NULL,2,1,2,2,1,1),(4,'Task pro jiny projekt','<p>sdfdsfdsfdsfdsfagsfgsdgfjasdgfhjkgasdkjfgaskjhdgfsdf</p>','2017-01-31 11:44:00.000000','2017-02-01 21:10:51.327000',3,3,4,10,13,8),(9,'Testovací tásk','<p>&yacute;&aacute;&iacute; &yacute;&scaron;&iacute;&aacute;?q&yacute;?&iacute;&aacute;?&scaron;&yacute;&eacute;?&aacute;&iacute;? &yacute;?&aacute;&iacute;&scaron;?&eacute;&yacute;? &eacute;&aacute;&iacute;?&yacute;&scaron;?&aacute;&iacute;? &yacute;?&scaron;&aacute;&iacute;&eacute;??&yacute; &iacute;?&scaron;?&yacute;? ?</p>','2014-01-28 01:12:00.000000',NULL,3,2,2,25,6,1),(10,'Testovací tásk 2 - test ','<p>akjfalksjdflajslkfjlaksj</p>','2017-02-01 15:34:00.000000','2017-02-01 21:48:23.217000',2,4,1,25,9,10),(11,'Testovací tásk 3','<p>ashdkjfhajksdhflkashdkjfha kjsdhfkashdkfahdsfhkjsdfkfsgdlkbsdsdnlkbfsdgkjbfsdkjghjkfsdgplkjsgdjfsdghiojsdglkjsdglkjsdg</p>','2017-02-01 15:57:00.000000','2017-02-01 18:25:22.610000',2,4,3,10,10,8),(12,'Testovací úkol pro testování','<p>Dneska testuji testovac&iacute; &uacute;kol pro testov&aacute;n&iacute;</p>','2017-02-01 18:47:00.000000',NULL,1,4,2,10,9,10),(13,'Projekt 8','<p>hasdjfhsdkjhfkjashflkhasdkjfhksh</p>','2017-02-01 21:11:20.189000',NULL,3,1,2,20,6,8);
/*!40000 ALTER TABLE `Task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Test`
--

DROP TABLE IF EXISTS `Test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Test` (
  `column_1` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Test`
--

LOCK TABLES `Test` WRITE;
/*!40000 ALTER TABLE `Test` DISABLE KEYS */;
/*!40000 ALTER TABLE `Test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Type`
--

DROP TABLE IF EXISTS `Type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Type` (
  `idType` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idType`),
  UNIQUE KEY `idType_UNIQUE` (`idType`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Type`
--

LOCK TABLES `Type` WRITE;
/*!40000 ALTER TABLE `Type` DISABLE KEYS */;
INSERT INTO `Type` VALUES (1,'Nová funkce','Nová funkce'),(2,'Bug','Bug'),(3,'Úprava funkce','Úprava funkce'),(4,'Servis','Servis');
/*!40000 ALTER TABLE `Type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `surname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `idUser_UNIQUE` (`idUser`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'test','$2a$04$8lF17eMr9/VeBawHb8tdMe28m2oPdZqgbnF4xZoY676bx1ff61Eie','Jan','Rydlo'),(8,'testUser','$2a$04$8lF17eMr9/VeBawHb8tdMe28m2oPdZqgbnF4xZoY676bx1ff61Eie','Test','User'),(9,'testManager','$2a$04$8lF17eMr9/VeBawHb8tdMe28m2oPdZqgbnF4xZoY676bx1ff61Eie','Test','Manager'),(10,'testAdmin','$2a$04$8lF17eMr9/VeBawHb8tdMe28m2oPdZqgbnF4xZoY676bx1ff61Eie','Test','Admin');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'pms'
--

--
-- Dumping routines for database 'pms'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-02  1:27:35
