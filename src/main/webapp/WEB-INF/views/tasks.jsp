<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
<h2>Seznam úkolů</h2>
<c:if test="${!empty listTasks}">

<form:form>
  <div class="row">
    <div class="col-lg-12">
      <label>Jméno: </label>
      <div class="row">
        <div class="col-xs-4">
          <div class="form-group">
            <input id="w-input-search" class="form-control" name="nameFiltr" type="text">
          </div>
        </div>
        <div class="col-xs-2">
          <div class="form-group">
           <button type="submit" class="btn btn-primary">Vyhledat</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form:form>
  
    <table class="table table-striped">
        <tr>
            <th width="120">Projekt - Název úkolu</th>
            <th width="60">Řešitel</th>
            <th width="60">Priorita úkolu</th>
            <th width="60">Status úkolu</th>
            <th width="60">Odhadovaná doba</th>
            <th width="60">Datum vytvoření</th>
            <th width="60">Datum ukončení</th>
                <th width="40"></th>
            <security:authorize access="hasAnyRole('ROLE_MANAGER','ROLE_ADMIN')">
                <th width="40"></th>
            </security:authorize>
        </tr>
        <c:forEach items="${listTasks}" var="task">
            <tr>
                <td><a href="<c:url value='/project/detail/${task.project.idProject}' />" >${task.project.name}</a> - <a href="<c:url value='/task/detail/${task.idTask}' />" >${task.name}</a></td>
                <td>${task.user.name} ${task.user.surname}</td>
                <td>${task.priority.name}</td>
                <td>${task.status.name}</td>
                <td>${task.estimatedTime}h</td>
                <td><fmt:formatDate value="${task.startDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                <td><fmt:formatDate value="${task.endDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                <td><a class="btn btn-info" href="<c:url value='/task/edit/${task.idTask}' />"  role="button">Upravit</a></td>
                <security:authorize access="hasAnyRole('ROLE_MANAGER','ROLE_ADMIN')">
                    <td><a class="btn btn-danger" href="<c:url value='/manage/task/remove/${task.idTask}'/>" role="button">Smazat</a></td>
                </security:authorize>
            </tr>
        </c:forEach>
    </table>
</c:if>
<a class="btn btn-primary" href="/task/create" role="button">Vytvořit úkol</a>
 <script>
 document.body.onload = function(){
	$('#w-input-search').autocomplete({
		 source: function (request, response) {
		        $.getJSON("/tasks/getNames?name=" + request.term, function (data) {
		        	console.log(data);
		            response($.map(data, function (value, key) {
		            	console.log(value);
		            	console.log(key);
		                return {
		                    label: value,
		                    value: key
		                };
		            }));
		        });
		    },
		    minLength: 2,
		    delay: 100
		});
 }
  </script>