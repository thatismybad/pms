<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
<h2>Seznam uživatelů</h2>
<c:if test="${!empty listUsers}">
    <table class="table table-striped">
        <tr>
            <security:authorize access="hasAnyRole('ROLE_ADMIN')">
                <th width="80">Uživatelské jméno</th>
            </security:authorize>
            <th width="80">Jméno</th>
            <th width="80">Příjmení</th>
            <security:authorize access="hasAnyRole('ROLE_ADMIN')">
                <th width="60">Role</th>
            </security:authorize>
            <security:authorize access="hasRole('ROLE_ADMIN')">
                <th width="40"></th>
                <th width="40"></th>
            </security:authorize>
        </tr>
        <c:forEach items="${listUsers}" var="user">
            <tr>
                <security:authorize access="hasAnyRole('ROLE_ADMIN')">
                    <td>${user.username}</td>
                </security:authorize>
                <td>${user.name}</td>
                <td>${user.surname}</td>
                <security:authorize access="hasAnyRole('ROLE_ADMIN')">
                    <td>${user.role}</td>
                </security:authorize>
                <security:authorize access="hasAnyRole('ROLE_ADMIN')">
                    <td><a class="btn btn-info" href="<c:url value='/admin/user/edit/${user.idUser}' />"  role="button">Upravit</a></td>
                    <c:choose>
                        <c:when test="${userAuth ne user.username}">
                            <td><a class="btn btn-danger" href="<c:url value='/admin/user/remove/${user.idUser}'/>" role="button">Smazat</a></td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                        </c:otherwise>
                    </c:choose>
                </security:authorize>
            </tr>
        </c:forEach>
    </table>
</c:if>
<security:authorize access="hasRole('ROLE_ADMIN')">
    <a class="btn btn-primary" href="/admin/user/create" role="button">Vytvořit uživatele</a>
</security:authorize>
