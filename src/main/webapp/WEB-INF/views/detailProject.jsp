<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>

<div class="container">
    <h2>Detail projektu</h2>
    <table class="table table-striped">
        <tr>
            <th>Projekt</th>
            <td>${projectForm.name}</td>
        </tr>
        <tr>
            <th>Vedoucí projektu</th>
            <td>${projectForm.user.name} ${projectForm.user.surname}</td>
        </tr>
        <tr>
            <th>Datum začátku</th>
            <td><fmt:formatDate value="${projectForm.startDate}" pattern="dd.MM.yyyy HH:mm"/></td>
        </tr>
        <tr>
            <th>Datum konec</th>
            <td><fmt:formatDate value="${projectForm.endDate}" pattern="dd.MM.yyyy HH:mm"/></td>
        </tr>
        <tr>
            <th>Popis</th>
            <td>${projectForm.description}</td>
        </tr>
    </table>
    
    <h2>Tasky k projektu</h2>
    <table class="table table-striped">
        <tr>
            <th width="120">Název úkolu</th>
            <th width="60">Řešitel</th>
            <th width="60">Priorita úkolu</th>
            <th width="60">Status úkolu</th>
            <th width="60">Odhadovaná doba</th>
            <th width="60">Datum vytvoření</th>
            <th width="60">Datum ukončení</th>
        </tr>
        <c:forEach items="${projectForm.tasks}" var="task">
            <tr>
                <td><a href="<c:url value='/task/detail/${task.idTask}' />" >${task.name}</a></td>
                <td>${task.user.name} ${task.user.surname}</td>
                <td>${task.priority.name}</td>
                <td>${task.status.name}</td>
                <td>${task.estimatedTime}h</td>
                <td><fmt:formatDate value="${task.startDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                <td><fmt:formatDate value="${task.endDate}" pattern="dd.MM.yyyy HH:mm"/></td>
            </tr>
        </c:forEach>
    </table>
</div>
<br/>
</html>