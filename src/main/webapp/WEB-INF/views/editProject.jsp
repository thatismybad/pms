<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>
<form:form method="POST" modelAttribute="projectForm" class="form-signin">
    <h2 class="form-signin-heading">Editace projektu</h2>
 
    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="name" class="control-label">Název projektu</label>
        <form:input type="text" path="name" class="form-control" placeholder="Název projektu" autofocus="true"></form:input>
        <form:errors path="name"></form:errors>
    </div>
    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="description" class="control-label">Popis projektu</label>
        	<form:textarea path="description" rows="8" cols="30" id="description"/>
        <form:errors path="description"></form:errors>
    </div>
     <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="user_id" class="control-label">Vedoucí projektu</label>
        <form:select class="form-control" id="user_id" path="user.idUser">
        	<c:forEach items="${userList}" var="user">
        		<form:option value="${user.idUser}" label="${user.name} ${user.surname}"></form:option>
        	</c:forEach>
        </form:select>
    </div>
	
	
    <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="type_id" class="control-label">Typ</label>
        <form:select class="form-control" id="type_id" path="type.idType" >
        	<c:forEach items="${typeList}" var="type">
        		<form:option value="${type.idType}" label="${type.name}"></form:option>
        	</c:forEach>
        </form:select>
    </div>
    
     <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="priority_id" class="control-label">Priorita</label>
        <form:select class="form-control" id="priority_id" path="priority.idPriority" >
        	<c:forEach items="${priorityList}" var="priority">
        		<form:option value="${priority.idPriority}" label="${priority.name}"></form:option>
        	</c:forEach>
        </form:select>
    </div>
    
      <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
        <label for="status_id" class="control-label">Status</label>
        <form:select class="form-control" id="status_id" path="status.idStatus" >
        	<c:forEach items="${statusList}" var="status">
        		<form:option value="${status.idStatus}" label="${status.name}"></form:option>
        	</c:forEach>
        </form:select>
    </div>
<form:hidden path="startDate" />
<form:hidden path="idProject" />
    <div class="form-group col-md-8">
        <button class="btn btn-primary" type="submit">Upravit projekt</button>
    </div>
</form:form>