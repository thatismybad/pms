<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglibs.jsp"%>

    <form:form method="POST" modelAttribute="userForm" class="form-signin">
        <h2 class="form-signin-heading">Vytvoření účtu</h2>

        <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
            <label for="name" class="control-label">Jméno</label>
            <form:input type="text" path="name" class="form-control" placeholder="Jméno" autofocus="true"></form:input>
            <form:errors path="name"></form:errors>
        </div>
        <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
            <label for="surname" class="control-label">Příjmení</label>
            <form:input type="text" path="surname" class="form-control" placeholder="Příjmení" autofocus="true"></form:input>
            <form:errors path="surname"></form:errors>
        </div>
        <spring:bind path="username">
            <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
                <label for="username" class="control-label">Uživatelské jméno</label>
                <form:input type="text" path="username" class="form-control" placeholder="Uživatelské jméno" autofocus="true"></form:input>
                <form:errors path="username"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
                <label for="password" class="control-label">Heslo</label>
                <form:input type="password" path="password" class="form-control" placeholder="Heslo"></form:input>
                <form:errors path="password"></form:errors>
            </div>
        </spring:bind>

        <div class="form-group ${status.error ? 'has-error' : ''} col-md-8">
          <label for="role" class="control-label">Role</label>
        <form:select path="role.idRole">
    		<form:options items="${rolesList}" itemValue="idRole" itemLabel="roleName"/>
		</form:select>
		</div>
        <div class="form-group col-md-8">
            <button class="btn btn-primary" type="submit">Vytvořit</button>
        </div>
    </form:form>
    </form>