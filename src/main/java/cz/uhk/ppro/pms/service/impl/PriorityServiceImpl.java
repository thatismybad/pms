package cz.uhk.ppro.pms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.entities.Priority;
import cz.uhk.ppro.pms.service.IPriorityService;

@Service
public class PriorityServiceImpl extends GenericServiceImpl<Priority, Integer> implements IPriorityService {

	
	@Autowired
    public PriorityServiceImpl(@Qualifier("priorityDaoImpl") IGenericDao<Priority, Integer> genericDao) {
    	super(genericDao);
    }
    
}
