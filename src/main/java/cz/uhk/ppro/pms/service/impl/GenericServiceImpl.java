package cz.uhk.ppro.pms.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.service.IGenericService;

@Service
public abstract class GenericServiceImpl<T, PK extends Serializable> implements IGenericService<T, PK>{

	protected IGenericDao<T, PK> genericDao;
	 
    public GenericServiceImpl(IGenericDao<T,PK> genericDao) {
        this.genericDao=genericDao;
    }
 
    public GenericServiceImpl() {
    }
	    
	    
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public PK create(T t) {
		return genericDao.create(t);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public T read(PK id) {
		return genericDao.read(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void update(T t) {
		genericDao.update(t);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(T t) {
		genericDao.delete(t);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void createOrUpdate(T t) {
		genericDao.createOrUpdate(t);	
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<T> getAll() {
		return genericDao.getAll();
	}

}
