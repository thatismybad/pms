package cz.uhk.ppro.pms.service;

import cz.uhk.ppro.pms.entities.Type;

public interface ITypeService extends IGenericService<Type, Integer> {

}
