package cz.uhk.ppro.pms.service;

import cz.uhk.ppro.pms.entities.Priority;

public interface IPriorityService extends IGenericService<Priority, Integer> {

}
