package cz.uhk.ppro.pms.service.impl;

import java.util.List;

import cz.uhk.ppro.pms.dao.IUserDao;
import cz.uhk.ppro.pms.service.ITaskService;
import cz.uhk.ppro.pms.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import cz.uhk.ppro.pms.dao.ICommentDao;
import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.entities.Comment;
import cz.uhk.ppro.pms.service.ICommentService;

@Service
public class CommentServiceImpl extends GenericServiceImpl<Comment, Integer> implements ICommentService {

	@Autowired
	private ICommentDao commentDao;

	@Autowired
	private IUserService userService;

	@Autowired
	private ITaskService taskService;
	
	@Autowired
    public CommentServiceImpl(@Qualifier("commentDaoImpl") IGenericDao<Comment, Integer> genericDao) {
    	super(genericDao);
    }

	@Override
	public Integer create(Comment comment) {
		String s = (String) SecurityContextHolder.getContext().getAuthentication().getName();
		comment.setTitle("");
		comment.setUser(userService.findByUsername(s));
		comment.setTask(taskService.read(comment.getTask().getIdTask()));
		return commentDao.create(comment);
	}

	@Override
	public List<Comment> findAllForTask(int idTask) {
		return commentDao.findAllForTask(idTask);
	}
    
}
