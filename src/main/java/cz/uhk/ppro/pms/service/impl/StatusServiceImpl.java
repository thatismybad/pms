package cz.uhk.ppro.pms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.entities.Status;
import cz.uhk.ppro.pms.service.IStatusService;

@Service
public class StatusServiceImpl extends GenericServiceImpl<Status, Integer> implements IStatusService{

	
	@Autowired
    public StatusServiceImpl(@Qualifier("statusDaoImpl") IGenericDao<Status, Integer> genericDao) {
    	super(genericDao);
    }
    
}
