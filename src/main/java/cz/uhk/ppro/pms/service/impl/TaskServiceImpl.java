package cz.uhk.ppro.pms.service.impl;

import java.util.Date;
import java.util.List;

import cz.uhk.ppro.pms.dao.IProjectDao;
import cz.uhk.ppro.pms.entities.*;
import cz.uhk.ppro.pms.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cz.uhk.ppro.pms.dao.IGenericDao;
import cz.uhk.ppro.pms.dao.ITaskDao;

@Service
public class TaskServiceImpl extends GenericServiceImpl<Task, Integer> implements ITaskService {

	@Autowired
	private ITaskDao taskDao;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IStatusService statusService;

	@Autowired
	private IPriorityService priorityService;

	@Autowired
	private ITypeService typeService;

	@Autowired
	private IUserService userService;
	
	@Autowired
    public TaskServiceImpl(@Qualifier("taskDaoImpl") IGenericDao<Task, Integer> genericDao) {
    	super(genericDao);
    }


	@Override
	public Integer create(Task task) {
		task.setProject(projectService.read(task.getProject().getIdProject()));
		task.setPriority(priorityService.read(task.getPriority().getIdPriority()));
		task.setUser(userService.read(task.getUser().getIdUser()));
		task.setType(typeService.read(task.getType().getIdType()));
		task.setStartDate(new Date());
		task.setStatus(statusService.read(1));

		return taskDao.create(task);
	}

	@Override
	public void update(Task task) {
		Status st = statusService.read(task.getStatus().getIdStatus());

		task.setProject(projectService.read(task.getProject().getIdProject()));
		task.setPriority(priorityService.read(task.getPriority().getIdPriority()));
		task.setUser(userService.read(task.getUser().getIdUser()));
		task.setType(typeService.read(task.getType().getIdType()));
		task.setStatus(st);
		if(st.getName().equals("Vyřešený") || st.getName().equals("Uzavřený")) {
			task.setEndDate(new Date());
		}

		taskDao.update(task);

	}

	@Override
	public List<Task> getAllForProject(int idProject) {
		return taskDao.getTasksForProject(idProject);
	}

	@Override
	public List<String> getTaskNames(String name) {
		return taskDao.getTaskNames(name);
	}

	@Override
	public List<Task> getTasksByName(String name) {
		return taskDao.getTaskByName(name);
	}

	@Override
	public List<Task> getTasksByUser(int idUser) {
		return taskDao.getTasksByUser(idUser);
	}

	@Override
	public List<Task> getActiveTasksByUser(int idUser, int idStatus) {
		return taskDao.getActiveTasksByUser(idUser, idStatus);
	}


}
