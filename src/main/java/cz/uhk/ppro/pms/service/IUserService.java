package cz.uhk.ppro.pms.service;

import java.util.List;

import cz.uhk.ppro.pms.entities.User;

public interface IUserService extends IGenericService<User, Integer> {
	
	/**
	 * Method finds user by his username
	 * @param username 
	 * @return returns {@link User} object
	 */
	 public User findByUsername(String username);
	 
	 /**
	  * Method finds all user with ADMIN or MANAGER role
	  * @return returns collection of {@link User} objects
	  */
	 public List<User> findAllManagersAndAdmins();
	 
}
