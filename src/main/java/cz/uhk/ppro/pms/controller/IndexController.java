package cz.uhk.ppro.pms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.uhk.ppro.pms.entities.User;
import cz.uhk.ppro.pms.service.IProjectService;
import cz.uhk.ppro.pms.service.ITaskService;
import cz.uhk.ppro.pms.service.IUserService;

@Controller
public class IndexController {

	@Autowired(required = true)
	private IProjectService projectService;
	@Autowired(required = true)
	private ITaskService taskService;
	@Autowired(required = true)
	private IUserService userService;
	
	 @RequestMapping(value = "/dashboard", method = RequestMethod.GET )
     public String prepareDashboard(Model model){
		 String s = (String)SecurityContextHolder.getContext().getAuthentication().getName();
		 User u = userService.findByUsername(s);
		 
        model.addAttribute("user", u);
        model.addAttribute("listProjects", projectService.getProjectsByUser(u.getIdUser()));    	
        model.addAttribute("listTasks", taskService.getActiveTasksByUser(u.getIdUser(),4));
        return "dashboard";
	 }
}
