package cz.uhk.ppro.pms.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import cz.uhk.ppro.pms.entities.Comment;
import cz.uhk.ppro.pms.entities.Priority;
import cz.uhk.ppro.pms.entities.Project;
import cz.uhk.ppro.pms.entities.Status;
import cz.uhk.ppro.pms.entities.Task;
import cz.uhk.ppro.pms.entities.Type;
import cz.uhk.ppro.pms.entities.User;
import cz.uhk.ppro.pms.service.ICommentService;
import cz.uhk.ppro.pms.service.IPriorityService;
import cz.uhk.ppro.pms.service.IProjectService;
import cz.uhk.ppro.pms.service.IStatusService;
import cz.uhk.ppro.pms.service.ITaskService;
import cz.uhk.ppro.pms.service.ITypeService;
import cz.uhk.ppro.pms.service.IUserService;
import cz.uhk.ppro.pms.validator.TaskValidator;

@SessionAttributes({"priorityList", "userList", "typeList", "statusList", "projectList"})
@Controller
public class TaskController {

    @Autowired(required = true)
    private ITaskService taskService;
    @Autowired(required = true)
    private IPriorityService priorityService;
    @Autowired(required = true)
    private ITypeService typeService;
    @Autowired(required = true)
    private IProjectService projectService;
    @Autowired(required = true)
    private IUserService userService;
    @Autowired(required = true)
    private IStatusService statusService;
    @Autowired(required = true)
    private ICommentService commentService;
    
    @Autowired
    private TaskValidator taskValidator;
    
    @RequestMapping(value = "/tasks", method = RequestMethod.GET )
    public String listTasks(Model model, Principal principal){
        model.addAttribute("tasks", new Task());
        model.addAttribute("listTasks", taskService.getAll());
        String s = (String) SecurityContextHolder.getContext().getAuthentication().getCredentials();
        System.out.println("Prihlaseny uzivatel: " + s);
        model.addAttribute("user", principal.getName());
        return "tasks";
    }
    
    @RequestMapping(value = "/tasks", method = RequestMethod.POST )
    public String listTasksFiltr(Model model, Principal principal, @RequestParam("nameFiltr") String nameFitltr){
        model.addAttribute("tasks", new Task());
        model.addAttribute("listTasks", taskService.getTasksByName(nameFitltr));
        String s = (String) SecurityContextHolder.getContext().getAuthentication().getCredentials();
        System.out.println("Prihlaseny uzivatel: " + s);
        model.addAttribute("user", principal.getName());
        return "tasks";
    }
    
    @RequestMapping(value = "/tasks/getNames", method = RequestMethod.GET)
	public @ResponseBody
	List<String> getNames(@RequestParam String name) {

		return taskService.getTaskNames(name);

	}
    
    @RequestMapping(value = "/tasks/project/{id}", method = RequestMethod.GET )
    public String listTasksForProject(Model model, @PathVariable("id") int id){
        model.addAttribute("tasks", new Task());
        model.addAttribute("listTasks", taskService.getAllForProject(id));
        return "tasks";
    }

    @RequestMapping(value = "/task/create", method = RequestMethod.GET)
    public String createTask(Model model) {
    	model.addAttribute("priorityList", priorityService.getAll());
    	model.addAttribute("typeList", typeService.getAll());
    	model.addAttribute("projectList", projectService.findAllActiveProjects());
    	model.addAttribute("userList", userService.findAllManagersAndAdmins());
        model.addAttribute("taskForm", new Task());
        return "createTask";
    }

    @RequestMapping(value = "/task/create", method = RequestMethod.POST)
    public String createTask(@ModelAttribute("taskForm") Task taskForm, BindingResult bindingResult) {
        taskValidator.validate(taskForm, bindingResult);
        
        if (bindingResult.hasErrors()) {
            return "createTask";
        }

        taskService.create(taskForm);
        return "redirect:/tasks";
    }


    @RequestMapping("/manage/task/remove/{id}")
    public String deleteTask(@PathVariable("id") int id){
        for(Comment comment : taskService.read(id).getComments()) {
            commentService.delete(comment);
        }
        taskService.delete(taskService.read(id));
        return "redirect:/tasks";
    }
    
    @RequestMapping(value = "/task/detail/{id}", method = RequestMethod.GET)
    public String detailTask(@PathVariable("id") int id, Model model){
    	model.addAttribute("taskForm", taskService.read(id));
    	model.addAttribute("commentList", commentService.findAllForTask(id));
    	model.addAttribute("commentForm", new Comment());
        return "detailTask";
    }

    @RequestMapping(value = "/task/edit/{id}", method = RequestMethod.GET)
    public String editTask(@PathVariable("id") int id, Model model){
    	
    	model.addAttribute("priorityList", priorityService.getAll());
    	model.addAttribute("typeList", typeService.getAll());
    	model.addAttribute("projectList", projectService.findAllActiveProjects());
    	model.addAttribute("userList", userService.findAllManagersAndAdmins());
        model.addAttribute("taskForm", taskService.read(id));
        model.addAttribute("listTasks", taskService.getAll());
        model.addAttribute("statusList", statusService.getAll());
        
        return "editTask";
    }
    
    @RequestMapping(value = "/task/edit/{id}", method = RequestMethod.POST )
    public String saveTask(@ModelAttribute("taskForm") Task taskForm, BindingResult bindingResult) {
    	
    	System.out.println(taskForm.getStartDate());
    	 taskValidator.validate(taskForm, bindingResult);
         
         if (bindingResult.hasErrors()) {
             return "editTask";
         }

        taskService.update(taskForm);
        return "redirect:/tasks";
    }
    
    @RequestMapping(value = "/task/detail/{id}", method = RequestMethod.POST)
    public String saveComment(@ModelAttribute("commentForm") Comment commentForm)  {

    	commentService.create(commentForm);
    	return "redirect:/task/detail/{id}";
    }
}
