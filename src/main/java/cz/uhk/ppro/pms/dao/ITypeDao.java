package cz.uhk.ppro.pms.dao;

import cz.uhk.ppro.pms.entities.Type;

public interface ITypeDao extends IGenericDao<Type, Integer>{
	
}
