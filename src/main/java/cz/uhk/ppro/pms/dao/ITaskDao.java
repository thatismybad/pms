package cz.uhk.ppro.pms.dao;

import java.util.List;

import cz.uhk.ppro.pms.entities.Task;

public interface ITaskDao extends IGenericDao<Task, Integer>{
	/**
	 * Method finds all task related to project
	 * @param idProject unique identifier of project
	 * @return returns collection of {@link Task}
	 */
	public List<Task> getTasksForProject(int idProject);
	
	/**
	 * Method find all task names by param
	 * @param name whole or partial name
	 * @return return collection of strings
	 */
	public List<String> getTaskNames(String name);
	
	/**
	 * Method finds all task with corresponding name
	 * @param name partial or whole name
	 * @return returns list of {@link Task}
	 */
	public List<Task> getTaskByName(String name);
	
	/**
	 * Method finds all task for user with corresponding identifier
	 * @param idUser unique identifier of User
	 * @return returns collection of {@link Task}
	 */
	public List<Task> getTasksByUser(int idUser);

	/**
	 * Method finds all task for user with corresponding identifier
	 * @param idUser unique identifier of User
	 * @return returns collection of {@link Task}
	 */
	public List<Task> getActiveTasksByUser(int idUser, int idStatus);
}
