package cz.uhk.ppro.pms.dao;

import java.util.List;

import cz.uhk.ppro.pms.entities.Project;

public interface IProjectDao extends IGenericDao<Project, Integer>{
	/**
	 * Method finds all active projects in DB
	 * @return return collection of found {@link Project}
	 */
	public List<Project> findAllActiveProjects();
	
	/**
	 * Method finds all corresponding names of projects
	 * @param name whole or partial project name
	 * @return returns collection of string
	 */
	public List<String> findProjectNames(String name);
	
	/**
	 * Method finds all project with corresponding name
	 * @param name whole or partial project name
	 * @return returns collection of found {@link Project}
	 */
	public List<Project> findProjectByName(String name);
	
	/**
	 * Method finds all project for user with id
	 * @param idUser unique identifier of User
	 * @return returns collection of {@link Project}
	 */
	public List<Project> findProjectByUser(int idUser);
}
