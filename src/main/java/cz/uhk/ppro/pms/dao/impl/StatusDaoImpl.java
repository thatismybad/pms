package cz.uhk.ppro.pms.dao.impl;

import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.IStatusDao;
import cz.uhk.ppro.pms.entities.Status;

@Repository
public class StatusDaoImpl extends GenericDaoImpl<Status, Integer> implements IStatusDao {
}
