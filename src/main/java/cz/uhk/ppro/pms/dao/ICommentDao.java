package cz.uhk.ppro.pms.dao;

import java.util.List;

import cz.uhk.ppro.pms.entities.Comment;

public interface ICommentDao extends IGenericDao<Comment, Integer>{
	/**
	 * Method finds all comments for task
	 * @param idTask unique identifier of task
	 * @return returns collection of {@link Comment}
	 */
	public List<Comment> findAllForTask(int idTask);
}
