package cz.uhk.ppro.pms.dao;

import cz.uhk.ppro.pms.entities.Status;

public interface IStatusDao extends IGenericDao<Status, Integer>{
	
}
