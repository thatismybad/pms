package cz.uhk.ppro.pms.dao;

import cz.uhk.ppro.pms.entities.Role;

public interface IRoleDao extends IGenericDao<Role, Integer> {
}
