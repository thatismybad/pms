package cz.uhk.ppro.pms.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.IProjectDao;
import cz.uhk.ppro.pms.entities.Project;

@Repository
public class ProjectDaoImpl extends GenericDaoImpl<Project, Integer> implements IProjectDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> findAllActiveProjects() {
		Criteria c = currentSession().createCriteria(Project.class);
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findProjectNames(String name) {
		Criteria c = currentSession().createCriteria(Project.class);
		if(name != null) {
			c.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
		}
		c.setProjection(Projections.property("name"));
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> findProjectByName(String name) {
		Criteria c = currentSession().createCriteria(Project.class);
		if(name != null) {
			c.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
		}
		return c.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> findProjectByUser(int idUser) {
		Criteria c = currentSession().createCriteria(Project.class);
		c.add(Restrictions.eq("user.idUser", idUser));
		return c.list();
	}

}
