package cz.uhk.ppro.pms.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import cz.uhk.ppro.pms.dao.ICommentDao;
import cz.uhk.ppro.pms.entities.Comment;

@Repository
public class CommentDaoImpl extends GenericDaoImpl<Comment, Integer> implements ICommentDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> findAllForTask(int idTask) {
		Criteria c = currentSession().createCriteria(Comment.class);
		c.add(Restrictions.eq("task.idTask", idTask));
		return c.list();
	}
	
}
