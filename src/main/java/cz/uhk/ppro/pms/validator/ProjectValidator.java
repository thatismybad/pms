package cz.uhk.ppro.pms.validator;

import cz.uhk.ppro.pms.entities.Project;
import cz.uhk.ppro.pms.service.IProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ProjectValidator implements Validator {

    @Autowired
    private IProjectService projectService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Project.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Project project = (Project) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        if (project.getName().length() < 6 || project.getName().length() > 200) {
            errors.rejectValue("name", "Size.projectForm.name");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "NotEmpty");
        if (project.getName().length() < 6 || project.getName().length() > 500) {
            errors.rejectValue("description", "Size.projectForm.description");
        }
        
    }
}
