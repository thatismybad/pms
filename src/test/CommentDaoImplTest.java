package cz.uhk.ppro.pms.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.uhk.ppro.pms.dao.ICommentDao;
import cz.uhk.ppro.pms.entities.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "test-context.xml"})
public class CommentDaoImplTest {

	@Autowired
	ICommentDao commentDao;
	
	@Test
	public void testFindAllForTask() {
		List<Comment> comments = commentDao.findAllForTask(0);
		assertTrue(comments.isEmpty());
		
		comments = commentDao.findAllForTask(1);
		assertTrue(!comments.isEmpty());
	}

}
