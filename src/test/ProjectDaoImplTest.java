package cz.uhk.ppro.pms.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cz.uhk.ppro.pms.dao.IProjectDao;
import cz.uhk.ppro.pms.entities.Project;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "test-context.xml"})
public class ProjectDaoImplTest {

	@Autowired
	private IProjectDao projectDao;
	
	@Test
	public void testFindAllActiveProjects() {
		List<Project> projects = projectDao.findAllActiveProjects();
		assertTrue(!projects.isEmpty());
	}

	@Test
	public void testFindProjectNames() {
		List<String> names = projectDao.findProjectNames(null);
		assertTrue(!names.isEmpty());
		
		names = projectDao.findProjectNames("p");
		assertTrue(!names.isEmpty());
		
		names = projectDao.findProjectNames("f");
		assertTrue(names.isEmpty());
	}

	@Test
	public void testFindProjectByName() {
		List<String> projects = projectDao.findProjectNames(null);
		assertTrue(!projects.isEmpty());
		
		projects = projectDao.findProjectNames("p");
		assertTrue(!projects.isEmpty());
		
		projects = projectDao.findProjectNames("f");
		assertTrue(projects.isEmpty());
	}

	@Test
	public void testFindProjectByUser() {
		List<Project> projects = projectDao.findProjectByUser(0);
		assertTrue(projects.isEmpty());
		
		projects = projectDao.findProjectByUser(1);
		assertTrue(!projects.isEmpty());
	}

}
